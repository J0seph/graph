import React from 'react';
import './App.css';
import TreView from './components/TreeView';
import { Segment, Accordion, Icon, Label, Button, Grid, Checkbox }
  from 'semantic-ui-react'
import { data } from './data';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeIndex: 0,
      bonusLider: 0,
      bonusTrimester: 0,
      isFortnight: false,
      isTrimester: false
    }
  };
  per = [3, 1.2, 0.12, 0.08];


  componentDidMount() {
    var jsonQ = require("jsonq");
    const people = jsonQ(data).find("name").value();
    people.forEach(element => {
      this.setState({ [element]: 0 });
      this.setState({ [element + 'own']: 0 });
      this.setState({ [element + 'fortnight']: 0 });
      this.setState({ [element + 'sales']: [] });
    });

  }

  handleChangeTrimester = (e, { checked }) => {
    this.setState({ isTrimester: checked });
    if (checked === true)
      this.calculateTrimesterGoal();
    else
      this.setState({ bonusTrimester: 0 });
  };

  handleChangeFortnight = (e, { checked }) => {
    this.setState({ isFortnight: checked });
    if (checked === true)
      this.calculateFortnightForAll();
    else
      this.dropFortnightForAll();
  };

  handleClick = (e, titleProps) => {
    const { index } = titleProps
    const { activeIndex } = this.state
    const nIndex = activeIndex === index ? -1 : index
    this.setState({ activeIndex: nIndex })
  };

  handleChange = ({ target }) => {
    this.setState({ [target.name]: target.value });
  };

  handleCalculate = (name, value) => {
    var jsonQ = require("jsonq");
    var person = jsonQ(data).find('name', function () {
      return this === name;
    });

    this.salesDirectRule(person, name, value, 0);
    this.calculateOwnGoal(person);
    if (this.state.isFortnight === true) this.calculateFortnight(name);
    this.calculateLiderGoal();
    if (this.state.isTrimester === true) this.calculateTrimesterGoal()
    this.setState({ [name]: 0 });

  };

  dropFortnightForAll = () => {
    var jsonQ = require("jsonq");
    const people = jsonQ(data).find("name").value();
    people.forEach(p => {
      this.setState({ [p + 'fortnight']: 0 });
    });
  }

  calculateLiderGoal = () => {
    var jsonQ = require("jsonq");
    const people = jsonQ(data).find("name").value();
    let total = 0;
    people.forEach(p => {
      const sales = this.state[p + 'sales'];
      sales.forEach(s => {
        total += Number(s.mount);
      });
    });
    if (total > 20000 && total <= 40000) this.setState({ "bonusLider": 150 });
    else if (total > 40000 && total <= 60000) this.setState({ "bonusLider": 250 });
    else if (total >= 80000) this.setState({ "bonusLider": 250 });

    console.log('total equipo', total);
  }

  calculateTrimesterGoal = () => {
    var jsonQ = require("jsonq");
    const people = jsonQ(data).find("name").value();
    let total = 0;
    people.forEach(p => {
      const sales = this.state[p + 'sales'];
      sales.forEach(s => {
        total += Number(s.mount);
      });
    });
    if (total > 45000 && total <= 65000) this.setState({ "bonusTrimester": 350 });
    else if (total > 65000 && total <= 85000) this.setState({ "bonusTrimester": 500 });
    else if (total > 85000 && total <= 100000) this.setState({ "bonusTrimester": 650 });
    else if (total > 100000) this.setState({ "bonusTrimester": 1000 });
  }


  calculateOwnGoal = (person) => {
    let sales = this.state[person.value()[0] + 'sales'];
    let sum = 0;
    sales.forEach(e => {
      sum += Number(e.mount);
    });
    if (sum >= 5000 && sum <= 10000) this.setState({ [person.value()[0] + 'own']: 150 })
    else if (sum >= 10000 && sum <= 15000) this.setState({ [person.value()[0] + 'own']: 200 })
    else if (sum >= 15000 && sum <= 25000) this.setState({ [person.value()[0] + 'own']: 300 })
    else if (sum > 25000) this.setState({ [person.value()[0] + 'own']: 400 })
    console.log('ownGoal', sum);
  }

  calculateFortnight = (name) => {
    let sales = this.state[name + 'sales'];
    let sum = 0;
    sales.forEach(e => {
      sum += Number(e.mount);
    });
    if (sum >= 5000) this.setState({ [name + 'fortnight']: 100 })
    console.log('fortnightGoal', sum);
  }

  calculateFortnightForAll = () => {
    var jsonQ = require("jsonq");
    const people = jsonQ(data).find("name").value();
    people.forEach(e => {
      this.calculateFortnight(e);
    });
  }


  salesDirectRule = (person, name, value, i) => {
    if (person.value().length === 0) return;

    let result = 0
    if (i === 0) {
      result = Number(value * (this.per[i] / 100));
      let sales = this.state[person.value()[0] + 'sales'];
      const data = { mount: value, com: result, refer: name }
      sales.push(data);
      this.setState({ [person.value()[0] + 'sales']: sales });
    }
    else {
      const realIndex = i >= this.per.length ? this.per.length - 1 : i
      result = Number(value * (this.per[realIndex] / 100));
      let sales = this.state[person.value()[0].name + 'sales'];
      const data = { mount: 0, com: result, refer: name }
      sales.push(data);
      this.setState({ [person.value()[0].name + 'sales']: sales });
    }
    i += 1;
    var father = person.closest('children').parent();
    return this.salesDirectRule(father, name, value, i);

  };


  renderSalesGrid = (sales) => {
    const renderSales = sales === undefined ? '' : sales.map((s, i) => {
      return (
        <Grid container columns={3} key={i}>
          <Grid.Column >{s.mount}</Grid.Column>
          <Grid.Column >
            {s.com}
          </Grid.Column>
          <Grid.Column >
            {s.refer}
          </Grid.Column>
        </Grid>);
    });
    return renderSales;
  };




  renderAccordion = (data) => {
    var jsonQ = require("jsonq");
    const people = jsonQ(data).find("name").value();


    return (
      <Accordion fluid styled>
        {
          people.map((name, i) => {
            return (
              <Segment key={i}>
                <Accordion.Title
                  active={this.state.activeIndex === i}
                  index={i}
                  onClick={this.handleClick}
                >
                  <Icon name='dropdown' />
                  {name}
                </Accordion.Title>
                <Accordion.Content active={this.state.activeIndex === i}>
                  <Segment>
                    <Label>Venta</Label>
                    <input type="number" name={name}
                      value={this.state[name]}
                      onChange={this.handleChange}>
                    </input>
                    <Button secondary onClick={() => this.handleCalculate(name, this.state[name])}>
                      Agregar
                    </Button>
                    <Segment>
                      <Label>Bono meta propia:</Label>
                      <Label circular color={'green'}>
                        {this.state[name + 'own']}
                      </Label>

                      <Label>Bono quincena:</Label>
                      <Label circular color={'green'}>
                        {this.state[name + 'fortnight']}
                      </Label>
                    </Segment>
                    <Segment>
                      <Grid container columns={3} key={i}>
                        <Grid.Column >{"Venta"}</Grid.Column>
                        <Grid.Column >
                          {"Comisión"}
                        </Grid.Column>
                        <Grid.Column >
                          {"Referencia"}
                        </Grid.Column>
                      </Grid>
                      {this.renderSalesGrid(this.state[name + 'sales'])}
                    </Segment>
                  </Segment>
                </Accordion.Content>
              </Segment>
            )
          })
        }
      </Accordion>
    )
  };

  render() {
    const accordion = this.renderAccordion(data);
    return (
      <div>
        <Segment>
          <TreView data={data}></TreView>
        </Segment>
        <Segment>
          <Label color='green' >Es quincena?</Label>
          <Checkbox checked={this.state.isFortnight} onChange={this.handleChangeFortnight} />

          <Label color='green' >Es trimeste?</Label>
          <Checkbox checked={this.state.isTrimester} onChange={this.handleChangeTrimester} />

        </Segment>
        <Segment>
          <Label>Bono liderazgo:</Label>
          <Label circular color={'green'}>
            {this.state.bonusLider}
          </Label>
          <Label>Bono trimestre:</Label>
          <Label circular color={'green'}>
            {this.state.bonusTrimester}
          </Label>

        </Segment>
        <Label color={'black'}>Miembros del equipo</Label>
        {accordion}


      </div>
    );
  }
}


