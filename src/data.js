export const data = {
    name: 'Michael',
    children: [{
        name: 'David',
        children: [{
            name: 'Freddie',
            children: [
                {
                    name: 'Jose'
                },
                {
                    name: 'Sandra',
                    children: [{ name: 'Tina' }
                    ]
                }
            ]
        },
        {
            name: "Prince"
        }
        ]
    }, {
        name: 'Phill',
        children: [
            {
                name: 'Toto',
                children: [
                    { name: 'Roxette' },
                    {
                        name: 'Abba',
                        children: [{ name: 'Ub 40' }]
                    }
                ]

            },
            { name: 'INXS' }
        ]
    }]
};